import kotlin.math.abs

fun main() {
    println(gcd(12, 18))
    println(gcd(16,20))
    println(gcd(120,900))
    println(gcd(105,26))

}


fun gcd(a: Int, b: Int): Int {
    var x = a
    var y = b

    if (x == 0) {
        return abs(y)
    }
    if (y == 0) {
        return abs(x)
    }

    var h: Int

    while (y != 0) {
        h = x.mod(y)
        x = y
        y = h
    }
    return abs(x)
}
